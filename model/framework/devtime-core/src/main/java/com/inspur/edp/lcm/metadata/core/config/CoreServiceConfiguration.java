/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.config;

import com.inspur.edp.lcm.metadata.core.event.DirEventBroker;
import com.inspur.edp.lcm.metadata.core.event.MetadataCreateEventBroker;
import com.inspur.edp.lcm.metadata.core.event.MetadataEventBroker;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Classname ServiceConfiguration Description TODO Date 2019/11/9 15:40
 *
 * @author zhongchq
 * @version 1.0
 */
//todo
//@Configuration(proxyBeanMethods = false)
@Configuration()
public class CoreServiceConfiguration {
    private EventListenerSettings settings;

    public CoreServiceConfiguration(EventListenerSettings settings) {
        this.settings = settings;
    }

    @Bean
    public MetadataEventBroker metadataEventBroker() {
        return new MetadataEventBroker(settings);
    }

    @Bean
    public MetadataCreateEventBroker metadataCreateEventBroker() {
        return new MetadataCreateEventBroker(settings);
    }

    @Bean
    public DirEventBroker dirEventBroker() {
        return new DirEventBroker(settings);
    }
}
