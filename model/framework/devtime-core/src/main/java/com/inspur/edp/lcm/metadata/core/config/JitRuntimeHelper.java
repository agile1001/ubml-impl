/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.config;

import com.inspur.edp.lcm.metadata.api.mvnEntity.JitRuntimeConfigration;
import com.inspur.edp.lcm.metadata.spi.JitRuntimeAction;

/**
 * @Classname JitCompilerHelper
 * @Description TODO
 * @Date 2019/7/29 16:06
 * @Created by zhongchq
 * @Version 1.0
 */
public class JitRuntimeHelper extends JitRuntimeConfigLoader {

    private static JitRuntimeHelper singleton = null;

    public JitRuntimeHelper() {
    }

    public static JitRuntimeHelper getInstance() {
        if (singleton == null) {
            singleton = new JitRuntimeHelper();
        }
        return singleton;
    }

    public JitRuntimeAction getManager() {
        JitRuntimeAction manager = null;
        JitRuntimeConfigration data = getJitRuntimeConfigration();
        if (data != null && data.getEnable()) {
            Class<?> cls = null;
            if (data.getAction() != null) {
                try {
                    cls = Class.forName(data.getAction().getName());
                    manager = (JitRuntimeAction) cls.newInstance();
                } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        return manager;
    }
}
