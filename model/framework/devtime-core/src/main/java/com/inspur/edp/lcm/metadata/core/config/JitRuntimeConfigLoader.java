/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.config;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.inspur.edp.lcm.metadata.api.mvnEntity.JitRuntimeConfigration;
import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import io.iec.edp.caf.common.environment.EnvironmentUtil;
import java.io.IOException;
import java.nio.file.Paths;
import lombok.Getter;
import lombok.Setter;

public class JitRuntimeConfigLoader {

    private static final String SECTION_NAME = "JitRuntimeConfigration";
    @Getter
    @Setter
    static JitRuntimeConfigration jitRuntimeConfigration;
    private static String fileName = "config/platform/common/lcm_gspprojectextend.json";

    public JitRuntimeConfigLoader() {
    }

    protected JitRuntimeConfigration getJitRuntimeConfigration() {

        fileName = Paths.get(EnvironmentUtil.getServerRTPath()).resolve(fileName).toString();

        try {
            if (jitRuntimeConfigration == null) {
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE);
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                FileServiceImp fileService = new FileServiceImp();
                String fileContents = fileService.fileRead(fileName);
                JsonNode jsonNode = objectMapper.readTree(fileContents);
                String jitRuntimeConfigrationJson = jsonNode.findValue(SECTION_NAME).toString();
                jitRuntimeConfigration = objectMapper.readValue(jitRuntimeConfigrationJson, new TypeReference<JitRuntimeConfigration>() {
                });
                return jitRuntimeConfigration;
            } else {
                return jitRuntimeConfigration;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
