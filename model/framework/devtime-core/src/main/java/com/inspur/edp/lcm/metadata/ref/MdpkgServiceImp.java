/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.ref;

import com.inspur.edp.lcm.metadata.api.entity.ProcessMode;
import com.inspur.edp.lcm.metadata.api.mvnEntity.MavenComponents;
import com.inspur.edp.lcm.metadata.api.mvnEntity.MavenPackageRefs;
import com.inspur.edp.lcm.metadata.api.mvnEntity.MetadataRepo;
import com.inspur.edp.lcm.metadata.api.service.MdpkgService;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.common.Utils;
import com.inspur.edp.lcm.metadata.core.PackageCoreService;
import com.inspur.edp.lcm.metadata.devcommon.ManagerUtils;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.io.IOException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

/**
 * 元数据服务
 */
@Slf4j
public class MdpkgServiceImp implements MdpkgService {

    private MetadataProjectService metadataProjectService;

    private MetadataProjectService getMetadataProjectService() {
        if (metadataProjectService == null) {
            metadataProjectService = SpringBeanUtils.getBean(MetadataProjectService.class);
        }
        return metadataProjectService;
    }

    private PackageCoreService packageCoreService = new PackageCoreService();

    /**
     * @param path    路径
     * @param modules 模块
     */
    @Override
    public void compile(String path, String modules) {
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        String mavenPath = ManagerUtils.getMavenStoragePath();
        packageCoreService.compile(absolutePath, modules, mavenPath);
    }

    @Override
    public void compileAll(String path) {
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        String mavenPath = ManagerUtils.getMavenStoragePath();
        packageCoreService.compile(absolutePath, "all", mavenPath);
    }

    @Override
    public void restore(String path, Boolean force) {
        if (Utils.isNullOrEmpty(path)) {
            throw new IllegalArgumentException("工程路径不能为空");
        }
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        String mavenPath = ManagerUtils.getMavenStoragePath();
        String packagePath = ManagerUtils.getMetadataPackageLocation();
        packageCoreService.restore(absolutePath, force, mavenPath, packagePath);
    }

    @Override
    public void addDepedencyAndRestore(String path, List<String> metadataPackages) {
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        String mavenPath = ManagerUtils.getMavenStoragePath();
        String packagePath = ManagerUtils.getMetadataPackageLocation();
        packageCoreService.addDependencyAndRestore(absolutePath, metadataPackages, mavenPath, packagePath);
    }

    @Override
    public void deployReleaseMdVersion(String path, String version, String repoId, boolean revert,
        String dependenciesVersion) throws IOException {
        Utils.checkEmpty(path, "工程路径不能为空");
        Utils.checkEmpty(path, "version不能为空");
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        String projPath = getMetadataProjectService().getProjPath(absolutePath);
        String mavenPath = ManagerUtils.getMavenStoragePath();
        final ProcessMode processMode = getMetadataProjectService().getProcessMode(projPath);
        switch (processMode.toString()) {
            case "interpretation":
                packageCoreService.deployMdpkgForInterpretation(projPath, repoId, version, dependenciesVersion, mavenPath);
                break;
            default:
                packageCoreService.deployReleaseMdVersion(projPath, version, repoId, revert, dependenciesVersion, mavenPath);
        }
    }

    @Override
    public void deploySnapshotMdVersion(String path, String repoId, String version, boolean revert) {
        Utils.checkEmpty(path, "工程路径不能为空");
        Utils.checkEmpty(path, "version不能为空");
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        String projPath = getMetadataProjectService().getProjPath(absolutePath);
        String mavenPath = ManagerUtils.getMavenStoragePath();

        final ProcessMode processMode = getMetadataProjectService().getProcessMode(projPath);
        switch (processMode.toString()) {
            case "interpretation":
                packageCoreService.deployMdpkgForInterpretation(projPath, repoId, version, null, mavenPath);
                break;
            default:
                packageCoreService.deploySnapshotMdVersion(projPath, repoId, version, revert, mavenPath);
        }
    }

    @Override
    public void installMdpkg(String groupId, String artifactId, String version, String proPath,
        Boolean forceUpdateFlag) throws IOException {
        // 创建pom文件,对应的mdproj添加依赖信息
        String absolutePath = ManagerUtils.getAbsolutePath(proPath);
        String mavenPath = ManagerUtils.getMavenStoragePath();
        String packagePath = ManagerUtils.getMetadataPackageLocation();
        packageCoreService.installMdpkg(groupId, artifactId, version, absolutePath, forceUpdateFlag, mavenPath, packagePath);
    }

    @Override
    public void uninstallMdpkg(String projPath, String packName) {
        String absolutePath = ManagerUtils.getAbsolutePath(projPath);
        String mavenPath = ManagerUtils.getMavenStoragePath();
        packageCoreService.uninstallMdpkg(absolutePath, packName, mavenPath);
    }

    @Override
    public void addProjectDependency(String pompAth, String groupId, String artifactId, String version) {
        packageCoreService.addProjectDependency(pompAth, groupId, artifactId, version);
    }

    @Override
    public void deployMdpkg(String proPath, String repoId, String dependencyVersion) {
        proPath = getMetadataProjectService().getProjPath(proPath);
        String mavenPath = ManagerUtils.getMavenStoragePath();
        packageCoreService.deployMdpkg(proPath, repoId, dependencyVersion, mavenPath);
    }

    @Override
    public MetadataRepo getRepoList() {
        return packageCoreService.getHostedRepoList();
    }

    @Override
    public String getMavenPackageLatestVersion(String groupId, String artifactId) {
        return packageCoreService.getMavenPackageLatestVersion(groupId, artifactId);
    }

    @Override
    public MavenPackageRefs getGAByMdpkgName(String mdpkgName) {
        return packageCoreService.getGAByMdpkgName(mdpkgName);
    }

    @Override
    public MavenComponents getVersionsOfSinglePackage(String sourceId, String packName) {
        MetadataRepo metadataRepo = getRepoList();
        return metadataRepo.getRepoList().stream().filter(repo -> repo.getId().equals(sourceId))
            .findFirst()
            .map(repo -> packageCoreService.getVersionsOfSinglePackage(repo.getSourceUrl(), packName))
            .orElse(null);
    }

    @Override
    public void batchOperate(String path, List<String> operations) {
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        String mavenPath = ManagerUtils.getMavenStoragePath();
        packageCoreService.batchOperation(absolutePath, mavenPath, operations);
    }
}
