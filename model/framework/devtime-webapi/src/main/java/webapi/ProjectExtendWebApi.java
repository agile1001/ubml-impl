/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package webapi;

import com.inspur.edp.lcm.metadata.api.entity.ExtractContext;
import com.inspur.edp.lcm.metadata.api.service.ProjectExtendService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProjectExtendWebApi {
    private ProjectExtendService projectExtendService;

    private ProjectExtendService getProjectExtendService() {
        if (projectExtendService == null) {
            projectExtendService = SpringBeanUtils.getBean(ProjectExtendService.class);
        }
        return projectExtendService;
    }

    /**
     * 批量编译当前目录下的所有工程
     *
     * @param path         路径
     * @param exts         扩展
     * @param disabledExts 禁用的扩展
     * @return
     */
    @POST
    public List<String> BatchCompile(@QueryParam("path") String path, @QueryParam("exts") String exts,
        @QueryParam("disabledExts") String disabledExts) {
        return getProjectExtendService().batchCompile(path, exts, disabledExts);
    }

    @PUT
    public void batchExtract(@QueryParam("path") String path) {
        getProjectExtendService().extract(path);
    }

    @PUT
    @Path("single")
    public void extract(ExtractContext context) {
        getProjectExtendService().extract(context.getProjectPath());
    }
}
