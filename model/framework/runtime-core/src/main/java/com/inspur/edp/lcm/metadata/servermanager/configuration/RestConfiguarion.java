/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.servermanager.configuration;

import com.inspur.edp.lcm.metadata.api.service.MetadataRTService;
import com.inspur.edp.lcm.metadata.rtservice.MetadataRTServiceImp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Classname RestConfiguarion Description TODO Date 2019/8/28 14:14
 *
 * @author zhongchq
 * @version 1.0
 */
//todo
//@Configuration(proxyBeanMethods = false)
@Configuration()
public class RestConfiguarion {

    @Bean
    public MetadataRTService createMetadataRTService() {
        return new MetadataRTServiceImp();
    }

//    @Bean
//    public MetadataRT4WebApiService createMetadata4WebApiService(){
//        return new MetadataRT4WebApiServiceImp();
//    }
//    @Bean
//    public RESTEndpoint restEndpoint(MetadataRT4WebApiService metadataRT4WebApiService){
//        return new RESTEndpoint("/runtime/sys/v1.0/rt-metadatas",metadataRT4WebApiService);
//    }
}
