/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.i18n.extractor;

import org.openatom.ubml.model.common.definition.cef.IFieldCollection;
import org.openatom.ubml.model.common.definition.cef.IGspCommonDataType;
import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.cef.i18n.context.CefResourcePrefixInfo;
import org.openatom.ubml.model.common.definition.cef.i18n.context.ICefResourceExtractContext;
import org.openatom.ubml.model.common.definition.cef.i18n.names.CefResourceDescriptionNames;
import org.openatom.ubml.model.common.definition.cef.i18n.names.CefResourceKeyNames;

public abstract class DataTypeResourceExtractor extends AbstractResourceExtractor {

    private IGspCommonDataType dataType;

    protected DataTypeResourceExtractor(IGspCommonDataType commonDataType, ICefResourceExtractContext context,
        CefResourcePrefixInfo parentResourceInfo) {
        super(context, parentResourceInfo);
        dataType = commonDataType;
    }

    protected final void extractItems() {

        addResourceInfo(CefResourceKeyNames.NAME, dataType.getName(), CefResourceDescriptionNames.NAME);
        extractDataTypeFields(dataType.getContainElements());
        //扩展
        extractExtendProperties(dataType);
    }

    protected final CefResourcePrefixInfo buildCurrentPrefix() {
        CefResourcePrefixInfo prefixInfo = new CefResourcePrefixInfo();
        if (getParentResourcePrefixInfo() == null) {
            throw new RuntimeException("多语项抽取_节点" + dataType.getName() + "所属模型，无对应的多语前缀信息。");
        }
        prefixInfo.setResourceKeyPrefix(getParentResourcePrefixInfo().getResourceKeyPrefix() + "." + dataType.getCode());
        prefixInfo.setDescriptionPrefix(getParentResourcePrefixInfo().getDescriptionPrefix() + "中'" + dataType.getName() + "'实体");
        return prefixInfo;
    }

    /**
     * 赋值节点的国际化项前缀
     */
    protected final void setPrefixInfo() {
        dataType.setI18nResourceInfoPrefix(getCurrentResourcePrefixInfo().getResourceKeyPrefix());
    }

    private void extractDataTypeFields(IFieldCollection fields) {
        if (fields == null || fields.size() == 0) {
            return;
        }
        for (IGspCommonField field : fields) {
            CefFieldResourceExtractor extractor = getCefFieldResourceExtractor(getContext(), getCurrentResourcePrefixInfo(), field);
            extractor.extract();
        }

    }

    /**
     * 获取字段抽取器
     *
     * @param context
     * @return
     */
    protected abstract CefFieldResourceExtractor getCefFieldResourceExtractor(ICefResourceExtractContext context,
        CefResourcePrefixInfo objPrefixInfo, IGspCommonField field);

    /**
     * 抽取扩展项
     *
     * @param dataType
     */
    protected void extractExtendProperties(IGspCommonDataType dataType) {
    }
}