/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.collection;

import org.openatom.ubml.model.common.definition.cef.operation.CommonValidation;
import org.openatom.ubml.model.common.definition.cef.util.Guid;

/**
 * The Collection Of Validation
 *
 * @ClassName: CommonValCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonValCollection extends BaseList<CommonValidation> implements Cloneable {
    private static final long serialVersionUID = 1L;

    public CommonValCollection clone(boolean isGenerateId) {
        CommonValCollection col = new CommonValCollection();
        for (CommonValidation var : this) {
            CommonValidation dtm = (CommonValidation)var.clone();
            if (isGenerateId) {
                dtm.setID(Guid.newGuid().toString());
            }
            col.add(dtm);
        }
        return col;
    }

    public CommonValCollection clone() {
        return clone(false);
    }
}